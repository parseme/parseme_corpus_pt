## Updating UD annotations to version 2.11

* Run by Carlos on Feb 07 2023

### GSD Corpus

0. `mkdir GSD; cd GSD`
1. Copy UD2.11 Portuguese GSD train here, rename it `pt_gsd-ud-train_ud2.11.conllu`
```
cp ~/Work/corpora/UD/ud-treebanks-v2.11/UD_Portuguese-GSD/pt_gsd-ud-train.conllu pt_gsd-ud-train_ud2.11.conllu
```
2. Generate a dummy CUPT containing empty MWEs (easier to copy-paste MWEless sentences):
```bash
awk '/^[0-9]/{printf("%s\t*\n",$0);}/^#/{print $0}/^$/{print}' pt_gsd-ud-train_ud2.11.conllu > pt_gsd-ud-train_ud2.11.dummycupt
```
3. Copy the PARSEME 1.2 CUPT file here, rename it `pt_gsd-ud-train_parseme1.2.cupt`
```bash
cp ../../../pt_gsd-ud-train.cupt pt_gsd-ud-train_parseme1.2.cupt
```
4. Copy UD script done in the past for correcting the _eo_ and _ea_ bug:
`cp ~/Work/corpora/UD/UD_Portuguese-GSD/scripts/correct-eo.py .`
5. Adapt it so that it uses `XPOS` and `UPOS` instead of `UPOSTAG` and `XPOSTAG`, and so that it does not change the parse tree
6. Generate a version of the cupt file with _eo_ and _ea_ auto re-tokenised:
```bash
./correct-eo.py pt_gsd-ud-train_parseme1.2.cupt > pt_gsd-ud-train_parseme1.2_eo-autocorrect.cupt
`` 
7. Run the alignment script and correct the remaining errors manually:
```bash
~/Work/develop/parseme/utilities/st-organizers/to_cupt.py --conllu pt_gsd-ud-train_ud2.11.conllu --input pt_gsd-ud-train_parseme1.2_eo-autocorrect.cupt --lang PT > pt_gsd-ud-train_parseme1.3.cupt
```

### Bosque corpus

0. `mkdir Bosque; cd Bosque`
1. Copy UD2.11 Portuguese Bosque here. Note: we use the Bosque source files, not the train/dev/test split. Concatenate all documents into a single `.conllu` file
```bash
cat ~/Work/corpora/UD/UD_Portuguese-Bosque/documents/* bosque-ud2.11-documents/CF* > bosque_cetenfolha_ud2.11.conllu
```
2. Generate a dummy CUPT containing empty MWEs (easier to copy-paste MWEless sentences):
```bash
awk '/^[0-9]/{printf("%s\t*\n",$0);}/^#/{print $0}/^$/{print}' bosque_cetenfolha_ud2.11.conllu > bosque_cetenfolha_ud2.11.dummycupt
```
3. Copy the PARSEME 1.2 CUPT file here, rename it `bosque_parseme1.2.cupt`
```bash
cp ../../../bosque.cupt bosque_parseme1.2.cupt
```
4. Run the alignment script and correct the remaining errors manually:
```bash
~/Work/develop/parseme/utilities/st-organizers/to_cupt.py --conllu bosque_cetenfolha_ud2.11.conllu --lang PT --input bosque_parseme1.2.cupt > bosque_parseme1.3.cupt
```

### Diário Gaucho corpus
