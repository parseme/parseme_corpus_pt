#!/bin/bash

NMAX=200
noutput=1
mkdir -p slices
rm -fr slices/*
for f in CF*.conllu; do
  noutputz=`printf "%03d" $noutput`
  echo $noutputz
  outfile="slices/bosque-slices-${noutputz}.conllu"
  echo $outfile
  cat $f >> $outfile
  nsent=`grep "# sent_id" $outfile | wc -l | awk '{print $1}'`
  echo "File $outfile has $nsent sentences"
  if [ $nsent -ge $NMAX ]; then
    (( noutput++ ))
  fi
done
