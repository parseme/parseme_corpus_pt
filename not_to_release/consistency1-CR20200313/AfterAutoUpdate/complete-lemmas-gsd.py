#!/usr/bin/env python3

# Scrpt used to replace the missing lemmas of GSD (in pt_gsd-ud-train.cupt, that is, the result of consistency checks) by automatically generated lemmas obtained by parsing GSD.

# GSD reparsing was obtained by the following command
# cd ~/Work/tools/parse/udpipe/src/
# ./src/udpipe training/models-ud-2.5/portuguese-bosque-ud-2.5-191206.udpipe \
#   --tag --input conllu ~/Work/develop/parseme/parseme_corpus_pt/not_to_release/gsd/pt_gsd-ud-train.conllu > ~/Work/develop/parseme/parseme_corpus_pt/not_to_release/gsd/pt_gsd-ud-train_reparsed2.5bosque-20200313.conllu

import pdb

with open("./pt_gsd-ud-train.cupt") as gsdorig :
  with open("../../../gsd/pt_gsd-ud-train_reparsed2.5bosque-20200313.conllu") as gsdparsed :
    for lineparsed in gsdparsed :
      fieldsparsed = lineparsed.strip().split("\t")
      lineorig = gsdorig.readline()      
      while lineorig.startswith("# metadata") :
        print(lineorig.strip())
        lineorig = gsdorig.readline()
      fieldsorig = lineorig.strip().split("\t")
      if (lineorig.startswith("#") or len(lineorig.strip()) == 0 ) :
        if lineorig == lineparsed or lineorig.startswith("# global") :
          print(lineorig.strip())
        else:
          pdb.set_trace()
      elif fieldsorig[0] == fieldsparsed[0] and fieldsorig[1] == fieldsparsed[1] :
        fieldsnew = fieldsorig.copy()
        fieldsnew[5] = fieldsparsed[5]
        if fieldsorig[2] == "_": # missing lemma, replace with parsed
          fieldsnew[2] = fieldsparsed[2]          
        print("\t".join(fieldsnew))         
      else:
        pdb.set_trace()
