#!/usr/bin/env python3

import pdb

with open("./diario_gaucho.cupt") as gsdorig :
  with open("../../../diario_gaucho/diario_gaucho_reparsed2.5bosque-20200313.conllu") as gsdparsed :
    for lineparsed in gsdparsed :
      fieldsparsed = lineparsed.strip().split("\t")
      lineorig = gsdorig.readline()      
      while lineorig.startswith("# metadata") :
        print(lineorig.strip())
        lineorig = gsdorig.readline()
      fieldsorig = lineorig.strip().split("\t")
      if (lineorig.startswith("#") or len(lineorig.strip()) == 0 ) :
        if lineorig == lineparsed or lineorig.startswith("# global") or (lineorig.startswith("# sent_id") and lineparsed.startswith("# sent_id")):
          print(lineorig.strip())   
          if lineorig.startswith("# global"):
            lineorig = gsdorig.readline()        
        elif lineorig.startswith("# text") and lineparsed.startswith("# text"):
          print(lineorig.strip())
        else:
          pdb.set_trace()
      elif fieldsorig[1] == fieldsparsed[1] : # and fieldsorig[0] == fieldsparsed[0] 
        fieldsnew = fieldsparsed.copy()        
        fieldsnew.append(fieldsorig[10])        
        print("\t".join(fieldsnew))       
      elif fieldsorig[1].startswith(fieldsparsed[1]) and fieldsorig[10] == "*":
        try:
          concatenation = fieldsparsed[1]
          while not fieldsorig[1] ==  concatenation:
            fieldsnew = fieldsparsed.copy()        
            fieldsnew.append(fieldsorig[10])        
            print("\t".join(fieldsnew))
            fieldsparsed = gsdparsed.readline().strip().split("\t")
            concatenation += fieldsparsed[1]
        except Exception:
          pdb.set_trace()
        fieldsnew = fieldsparsed.copy()        
        fieldsnew.append(fieldsorig[10])        
        print("\t".join(fieldsnew))
      elif fieldsparsed[1].startswith(fieldsorig[1]) and fieldsorig[10] == "*":        
        #pdb.set_trace()
        fieldsnew = fieldsparsed.copy()
        fieldsnew.append(fieldsorig[10])        
        print("\t".join(fieldsnew))
        while not fieldsparsed[1].endswith(fieldsorig[1]) :
          fieldsorig = gsdorig.readline().strip().split("\t")
          if fieldsorig[10] != "*": # problems: MWEs on different tokens
            pdb.set_trace()                      
      else:        
        pdb.set_trace()
